package com.example.tinylog_ohos;

import org.junit.Test;
import org.pmw.tinylog_test.Configurator;
import org.pmw.tinylog_test.Level;
import org.pmw.tinylog_test.Logger;

public class ExampleTest {
    @Test
    public void onStart() {
        Logger.info("Test        info测试");
        Logger.error("Test        error测试");
        Configurator.currentConfig().level(Level.DEBUG).activate();
        Logger.debug("Test        debug测试");
        Configurator.currentConfig().level(Level.TRACE).activate();
        Logger.trace("Test        trace测试");
        Logger.warn("Test        warn测试");
    }
}
