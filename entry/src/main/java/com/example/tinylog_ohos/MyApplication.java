package com.example.tinylog_ohos;

import ohos.aafwk.ability.AbilityPackage;
import org.pmw.tinylog_test.Logger;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        Logger.info("MyApplication info Log print !");
    }
}
