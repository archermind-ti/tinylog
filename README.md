# Tinylog_ohos

### 项目介绍

简化日志打印的日志打印库

### 安装教程

方式一

1. 下载[Release](https://gitee.com/archermind-ti/tinylog/releases/)下`tinylog.jar`  `slf4j_binding.jar`  `log4j_facade.jar`

2. 启动 DevEco Studio，将下载的jar包，导入工程目录“entry->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下jar包的引用。
```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

### 使用说明
```
import org.tinylog.Logger;
    
public class Application {

    public static void main(String[] args) {
        Logger.info("Hello World!");
    }

}
```

### 功能示例

- testLoggingLevel

  ![](1.png)

- testCustomLoggingLevel

  ![](2.png)

- testTrace

  ![](3.png)

- testDebug

  ![](4.png)

### 版本迭代

* v1.0.0

### License
```
Copyright 2016-2021 Martin Winandy

Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, 
software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
either express or implied. 
See the License for the specific language governing permissions and limitations under the License.
```